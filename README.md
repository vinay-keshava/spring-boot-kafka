## Kafka with Spring Boot

- Producer: User Service
- Consumer: SMS Service,Account Service


Producer i.e User Service, registers a profile and this message event is produced to kafka, multiple consumers(in this case SMS service and Account service)  consumes this message event and SMS service sends a profile confirmation SMS to the phone number registered, while account service creates an account for the particular user.


##### Instructions to run the project

Set the below System Environment variables to run the project.

`MYSQL_USERNAME`: user name of the mysql server

`MYSQL_PASSWORD`: password for the mysql user

`MYSQL_URL`: url and port number of the mysql server connection

`TWILIO_NUMBER`: Mobile number given by twilio

`TWILIO_ACCOUNT_SID`: Twilio account SID

`TWILIO_AUTH_TOKEN`: Authentication Token provided by Twilio
