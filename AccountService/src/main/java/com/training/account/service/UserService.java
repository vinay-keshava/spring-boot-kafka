package com.training.account.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.training.account.dto.UserDto;

@FeignClient(url = "http://localhost:9000/api/v1/users",name="user-service")
public interface UserService {

	@GetMapping(value = "/{userId}")
	UserDto getUser(@PathVariable Integer userId);
}
