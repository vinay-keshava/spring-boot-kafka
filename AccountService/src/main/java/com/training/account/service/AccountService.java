package com.training.account.service;

import java.io.IOException;
import java.security.SecureRandom;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.BeanUtils;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.account.dto.AccountFeignResponse;
import com.training.account.entity.Accounts;
import com.training.account.repository.AccountsRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountService {

	private final AccountsRepository accountsRepository;

	@KafkaListener(topics = "usertopic",groupId = "userId")
	public void consume(ConsumerRecord<String, Object> records) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(records.value().toString());
		String email = node.get("email").asText();

		SecureRandom secureRandom = new SecureRandom();
		Accounts accounts = new Accounts();
		accounts.setAccountBalance(1000.00);
		accounts.setAccountName("Savings");
		accounts.setAccountNumber(Math.abs(secureRandom.nextLong()));
		accounts.setEmail(email);

		accountsRepository.save(accounts);
		log.info(String.format("Order event received in stock service => %s", records.toString()));
	}

	public AccountFeignResponse getAccount(Long accountNumber) {

		Accounts accounts = accountsRepository.findByAccountNumber(accountNumber);
		AccountFeignResponse accountFeignResponse = new AccountFeignResponse();
		BeanUtils.copyProperties(accounts, accountFeignResponse);

		return accountFeignResponse;
	}

}
