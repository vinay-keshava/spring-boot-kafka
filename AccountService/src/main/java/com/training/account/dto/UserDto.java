package com.training.account.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {

	String userName;
	String password;	
	Long phoneNumber;
	String email;
}
