package com.training.account.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountFeignResponse {
	
	private String accountName;
	private Long accountNumber;
	private Double accountBalance;
	private String email;
}
