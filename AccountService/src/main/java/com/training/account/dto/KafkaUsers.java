package com.training.account.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KafkaUsers {
	
	private String userId;
	private String userName;
	private String password;
	private String phoneNumber;
	private String email;
}
