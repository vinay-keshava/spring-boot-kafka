package com.training.account.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ResponseDto {

	String message;
	int code;
}
