package com.training.account.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountDto {

	Integer userId;
	String accountName;
	Long accountNumber;
	Double accountBalance;
	
}
