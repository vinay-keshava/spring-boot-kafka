package com.training.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.account.entity.Accounts;

@Repository
public interface AccountsRepository extends JpaRepository<Accounts, Long>{

	Accounts findByAccountNumber(Long accountNumber);

}
