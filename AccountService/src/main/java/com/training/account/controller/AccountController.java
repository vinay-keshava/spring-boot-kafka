package com.training.account.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.training.account.dto.AccountDto;
import com.training.account.dto.AccountFeignResponse;
import com.training.account.entity.Accounts;
import com.training.account.service.AccountService;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class AccountController {

	private final AccountService accountService;

//	@PostMapping("/accounts")
//	public ResponseEntity<ResponseDto> newAccount() {
//		return new ResponseEntity<>(accountService.consume(), HttpStatus.OK);
//	}
	
	@GetMapping("/accounts/{accountNumber}")
	public ResponseEntity<AccountFeignResponse> findByAccountNumber(@PathVariable Long accountNumber){
		return new ResponseEntity<>(accountService.getAccount(accountNumber),HttpStatus.OK);
	}

	
	
//	@PostMapping("path")
//	public SomeEnityData postMethodName(@RequestBody SomeEnityData entity) {
//		//TODO: process POST request
//		
//		return entity;
//	}
	
	
}
