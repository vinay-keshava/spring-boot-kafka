package com.training.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.user.entity.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer>{

	Users findByUserId(Integer userId);

}
