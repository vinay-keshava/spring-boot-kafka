package com.training.user.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {

	@NotNull(message = "userName is mandatory")
	String userName;
	
	@NotNull(message = "userName is mandatory")
	String password;
	
	Long phoneNumber;

	@Email(message = "Email is mandatory")
	String email;
}
