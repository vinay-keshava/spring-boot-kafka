package com.training.user.service;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.training.user.dto.ResponseDto;
import com.training.user.dto.UserDto;
import com.training.user.entity.Users;
import com.training.user.repository.UsersRepository;
import com.training.user.serializer.JsonSerializer;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserService {

	private final UsersRepository userRepository;

	public ResponseDto newUser(UserDto dto) {

		Properties properties = new Properties();
		properties.put("bootstrap.servers", "localhost:9092");

		@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
		Producer<String, Users> producer = new KafkaProducer<>(properties, new StringSerializer(),
				new JsonSerializer());
		Users users = new Users();
		BeanUtils.copyProperties(dto, users);
		ProducerRecord<String, Users> records = new ProducerRecord<>("usertopic", users);
		producer.send(records);
		userRepository.save(users);
		
		return ResponseDto.builder().message("user registration successfull").httpStatusCode(200).build();
	}

	public UserDto getUser(Integer userId) {
		Users users = userRepository.findByUserId(userId);
		UserDto dto = new UserDto();
		BeanUtils.copyProperties(users, dto);
		return dto;
	}
}
