package com.training.user.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.user.dto.ResponseDto;
import com.training.user.dto.UserDto;
import com.training.user.service.UserService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class UserController {

	UserService userService;

	public UserController(UserService userService) {
		super();
		this.userService = userService;
	}

	@PostMapping("/users")
	public ResponseEntity<ResponseDto> newUser(@Valid @RequestBody UserDto userDto) {
		return new ResponseEntity<>(userService.newUser(userDto),HttpStatus.ACCEPTED);
	}

	@GetMapping("/users/{userId}")
	public ResponseEntity<UserDto> getUser(@Valid @PathVariable Integer userId) {
		return new ResponseEntity<>(userService.getUser(userId),HttpStatus.OK);
	}
}
