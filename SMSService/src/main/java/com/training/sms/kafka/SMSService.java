package com.training.sms.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SMSService {

	@KafkaListener(groupId = "userId", topics = "usertopic")
	public void consumeUserAndSendSms(ConsumerRecord<String, Object> records)
			throws JsonMappingException, JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(records.value().toString());
		String email = node.get("email").asText();
		String phoneNumber=node.get("phoneNumber").asText();
		System.out.println(email);
		log.info(email);
		String fromNumber = System.getenv("TWILIO_NUMBER");
		Twilio.init(System.getenv("TWILIO_ACCOUNT_SID"), System.getenv("TWILIO_AUTH_TOKEN"));
		Message.creator(new PhoneNumber("+91"+phoneNumber), new PhoneNumber(fromNumber),
				"Your profile has been created successfully at ABC Bank with email " + email).create();
	}
}
