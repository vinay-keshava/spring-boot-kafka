package com.training.sms.deserializer;

import java.io.IOException;

import org.apache.kafka.common.serialization.Deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonDeserializer<T> implements Deserializer<Object>{

	
	public JsonDeserializer(Class<T> type) {
		this.type = type;
	}
	public JsonDeserializer() {
		}

	private Class<T> type;

	@Override
	public Object deserialize(String topic, byte[] data) {
		ObjectMapper mapper = new ObjectMapper();
		T object = null;
		try {
			object = mapper.readValue(data, type);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return object;
	}
	
}